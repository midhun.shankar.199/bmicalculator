let buttonCalculate = document.getElementById("calculateButton")
buttonCalculate.addEventListener("click", calculate)

let buttonReset = document.getElementById("resetButton")
buttonReset.addEventListener("click", reset)

function calculate() {
    let weight = document.getElementById("weight").value
    let height = document.getElementById("height").value
    let result = document.getElementById("bmiOutputDisplay")
    let verdict = document.getElementById("bmiVerdict")

   if (height === "" || isNaN(height)) {
        result.innerHTML = "Invalid!"
        result.style.color = "orange"
   } else if (weight === "" || isNaN(weight)) {
        result.innerHTML = "Invalid!"
        result.style.color = "orange"
   } else {
        let bmi = (weight / (height * height)).toFixed(2)
        if (bmi >= 30){
         result.innerHTML = bmi
         verdict.innerHTML = "Obese"
         verdict.style.color = "red"
        } else if (bmi >= 25 && bmi < 30) {
            result.innerHTML = bmi
            verdict.innerHTML = "Overweight"
            verdict.style.color = "orange"
        } else if (bmi >= 18.5 && bmi < 25) {
            result.innerHTML = bmi
            verdict.innerHTML = "Healthy"
            verdict.style.color = "#36A9B1"
        } else {
            result.innerHTML = bmi
            verdict.innerHTML = "Underweight"
            verdict.style.color = "#a5a5a5"
        }
   }
}

function reset() {
   document.getElementById("weight").value = ""
   document.getElementById("height").value = ""
   let result = document.getElementById("bmiOutputDisplay")
   result.innerHTML = 0
   result.style.color = "#36A9B1"
   let verdict = document.getElementById("bmiVerdict")
   verdict.innerHTML = ""
}